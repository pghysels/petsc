#if !defined(_PETSCLOGDEFAULT_H)
  #define _PETSCLOGDEFAULT_H

  #include <petsc/private/loghandlerimpl.h> /*I "petscsys.h" I*/
  #include <petsc/private/logimpl.h>        /*I "petscsys.h" I*/

PETSC_INTERN PetscErrorCode PetscLogHandlerCreate_Default(PetscLogHandler);

#endif // #define _PETSCLOGDEFAULT_H
