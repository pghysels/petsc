#ifndef PETSCXMLVIEWER_H
#define PETSCXMLVIEWER_H

#include "lognested.h"

PETSC_INTERN PetscErrorCode PetscLogView_Nested_XML(PetscLogHandler_Nested, PetscNestedEventTree *, PetscViewer);

#endif
